hi Krakenite o/

# build

TL;DR

	bash build.sh

# litecoin image
	wget https://raw.githubusercontent.com/uphold/docker-litecoin-core/master/0.18/Dockerfile
	wget https://raw.githubusercontent.com/uphold/docker-litecoin-core/master/0.18/docker-entrypoint.sh

dockerfile base and entrypoint (not modified) borrowed here

after I removed the entrypoint because I didn't needed it

# signature check
it's already in the Dockerfile with the gpg check

# note about vuln check with anchore

I had some issues with anchore, and honestly discovered the tool in the test.
It was finding this CVE on sqlite
https://security-tracker.debian.org/tracker/CVE-2020-15358

I tested several things to be sure to have a good understanding of anchore cli etc..

I first followed that article 

https://anchore.com/blog/inline-scanning-with-anchore-engine/

## patching
first it took forever to install, and the rm all
the next day I decided to remove the `docker rm` from the inline_scan script
It saved me such minutes to run my tests

## vuln ?
but the scanner version v0.6.0 referenced was finding vulnerabilities on libsqlite3-0

	HIGH Vulnerability found in os package type (dpkg) - libsqlite3-0 (CVE-2020-15358 - https://security-tracker.debian.org/tracker/CVE-2020-15358)          stop

after checked the CVE, I found version libsqlite3-0_3.27.2-3+deb10u1_amd64.deb present in that debian image was already patched

I double check the package version with dpkg -l during the build.

So anchore should be the problem.

I even check quickly the anchore code on github and see they where using dpkg to check package versions

but even if I tryed to remove the package from the image, the vuln was still detected, maybe anchore v0.6 was behaving differently

I checked the anchore code, and dig up a little more,
https://anchore.com/blog/how-many-cves/
interesting but in that case the tool is a bit useless...

## upgrade
Eventually I found there was other versions of their inline_scan in the gitlab example on the enterprise version

I prayed a bit hoping open source version has access to this script to, and yes it had.

I upgraded to latest anchore verssion and rerun the tests, besides it took 30Gb and many minutes to build run and download, this time it was ok

I got only warnings. :)

# security
the only changes I made was the entryp oint and the user
and a chown obivously so the litecoind can run without root.

# docker hub
https://docs.docker.com/docker-hub/repos/

thekit/litecoin uploaded
