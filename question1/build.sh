#!/bin/bash

set -exv

IMAGE=litecoin
TAG=0.18.1
docker build -t "$IMAGE:$TAG" .

# curl -s https://ci-tools.anchore.io/inline_scan-v0.6.0 | bash -s -- -V -f -d Dockerfile litecoin:0.18.1
# bash inline_scan-v0.6.0 -V -f -d Dockerfile litecoin:0.18.1
# curl -s https://ci-tools.anchore.io/inline_scan-latest | bash -s -- -V -f -d Dockerfile litecoin:0.18.1
if ! bash inline_scan-latest -V -f -d Dockerfile "$IMAGE:$TAG";then
	docker tag litecoin "thekit/$IMAGE:$TAG"
	docker push "thekit/$IMAGE:$TAG"
else
	echo sorry security checkup failed
fi
