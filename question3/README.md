# CI
the anchore example script is a very good place to start for a simple CI

on variables in gitlab it has to be set up here
https://docs.gitlab.com/ee/ci/variables/

gitlab run its CI in docker alpine containers so we have to add curl etc...


# CI/CD
add variable for docker login to settings CI/CD masked variable
CI_REGISTRY_PASSWORD

follow this to use dockerhub and not gitlab registry
https://stackoverflow.com/questions/45517733/how-to-publish-docker-images-to-docker-hub-from-gitlab-ci

[../.gitlab-ci.yml](../.gitlab-ci.yml)

I fixed this line 

    - if [[ "$f" =~ "content-os" ]]; then
    + if [[ "$f" == *"content-os"* ]]; then

because the bash in gitlab's image does not handle `=~` and using regexp to match static words is not good for perfomance.
