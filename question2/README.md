# about the image
I've gone for mypublic repo thekit/litecoin on dockerhub

but I would have use ECR or any private docker registry in production

I used helm (3) because I think it was more interesing

first I ran

	helm3 create litecoin

and hacked into it
- put everything in a namespace "kraken"
- added a script to deploy easily
- change deployment to statefulset
- added persistent volume
- added persistent volume claim
- added local-storageclass
- added ressource limits
- added volume to the statefulset

# deploy with

	cd question2/k8s/litecoin/
	sh install.sh

# delete with

	helm3 delete -n kraken litecoin-kraken

# readyness attempt
first added readyness check, then disabled it because I'm not sure how the litecoin daemon should answer and if it is compatible with k8s TCP test
and it is restarting the daemon because it cannot connect to port 9333
https://kubernetes.io/docs/tasks/configure-pod-container/configure-liveness-readiness-startup-probes/#define-a-tcp-liveness-probe

# volumes
used part of these

volume k8s local

https://kubernetes.io/docs/concepts/storage/volumes/#local

https://kubernetes.io/docs/concepts/storage/persistent-volumes/#persistentvolumeclaims

https://kubernetes.io/docs/concepts/storage/storage-classes/#local

and this
https://kubernetes.io/docs/concepts/workloads/controllers/statefulset/

local storage is not my prefered choice, but it's the easier to be minikube compatible

I would have gone for efs with aws I think, if performance was not an issue with litecoin.

some nice doc I found
https://medium.com/faun/deploying-statefulsets-in-kubernetes-k8s-5924e701d327

# about cpu limits
I've activated the default ones but I got this video in mind https://youtu.be/eBChCFD9hfs?t=1493 and other articles.

Cpu limits are bad for k8s performance it make cpu throttling, google recommend to disable it etc...

(watch the full video for explanations and there is multiple articles about that too)

As it was getting oom killed, so I raised the memory by 10 times to 1280 MB and cpu to 500m to give it some air.


# interesting resource and litecoind test
https://medium.com/@baddour/setting-up-a-litecoin-full-node-a-complete-step-by-step-recipe-that-anyone-can-follow-fb18b82d5cfe

I've check a little this article and ran some commands, I didn't know if the litecoind was running really ok, but it gives me some blockchain info

at start it was not moving a lot and after letting it run for some hours without looking

every time I ran `litecoin-cli getblockchaininfo`, I got some new hashes so I think it's ok

maybe it's still syncing the chain info or the new transfert that arrive in real time.

start a shell into the image (k9s + s)

	litecoin-cli getblockchaininfo

