# iam role

# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_role
#

resource "aws_iam_role" "test_role" {
  name = local.iam_role_name

  assume_role_policy = jsonencode({
    Version = "2012-10-17",
    Statement = [
      {
        Effect    = "Allow",
        Action    = "sts:AssumeRole",
        Principal = { "AWS" : "arn:aws:iam::${data.aws_caller_identity.current.account_id}:*" }
    }]
  })
  tags = {
    prod = "true"
  }
}


# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_policy
# https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_use_passrole.html
# https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_use_permissions-to-switch.html
resource "aws_iam_policy" "assume_policy" {
  name        = local.iam_policy_name
  description = "assume role for users in this account"

  # Terraform's "jsonencode" function converts a
  # Terraform expression result to valid JSON syntax.
  policy = jsonencode({
    Version = "2012-10-17"
    Statement = [
      {
        Action = [
          "iam:PassRole",
        ]
        Effect   = "Allow"
        Resource = "arn:aws:iam::${data.aws_caller_identity.current.account_id}:role/${local.iam_role_name}"
      },
    ]
  })
}

# iam group
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group
resource "aws_iam_group" "krakenites" {
  name = local.iam_group_name

}

# iam user
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_user
resource "aws_iam_user" "test_user" {
  name = local.iam_user_name

  tags = {
    prod = "true"
  }
}


# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_policy_attachment
#
resource "aws_iam_group_policy_attachment" "test-attach" {
  group      = aws_iam_group.krakenites.name
  policy_arn = aws_iam_policy.assume_policy.arn
}

# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/iam_group_membership
# group attachment
resource "aws_iam_group_membership" "team" {
  name = "tf-testing-group-membership"

  users = [
    aws_iam_user.test_user.name,
  ]

  group = aws_iam_group.krakenites.name
}

