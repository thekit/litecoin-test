# module variables
#

variable "name" {
  type = string
  // no default so it will crash
}

variable "suffix" {
  type    = bool
  default = false
}

locals {
  iam_role_name   = (var.suffix ? "${var.name}-role" : var.name)
  iam_policy_name = (var.suffix ? "${var.name}-policy" : var.name)
  iam_group_name  = (var.suffix ? "${var.name}-group" : var.name)
  iam_user_name   = (var.suffix ? "${var.name}-user" : var.name)
}

# https://www.terraform.io/docs/language/expressions/conditionals.html
