#include <unistd.h>
#include <fcntl.h>
#include <string.h>

void show_string(char *buf){
	char Bound[] = "Bound ";
	write(1, Bound, 6);

	// search for "to " and print it
	char *buf_to = strstr(buf, "to ");
	if (buf_to > 0){
		// skip the 3 char "to "
		write(1, buf_to + 3 * sizeof(char), strlen(buf_to) - 3);
	}
	
	char at[] = " at ";
	write(1, at, 4);

	//seek the time only
	int show = 0;
	for (int i = 0;buf[i]; ++i){
		if (buf[i-1] == 'T')
			show = 1;
		if(show) write(1, buf + i * sizeof(char), 1);
		if (buf[i+1] == 'Z'){
			write(1, "\n", 1);
			show = 0;
		}
			
	}
}

int main(){
	char c;
	unsigned int n;
	char buf[1024];

	int fd = open("./logs_from_ltc.log", O_RDONLY);
	for(int i = 0, ix = 0;(n = read(fd, &c, 1)); ++i){
		if (c == '\n'){
			buf[ix] = '\0';
			if (strstr(buf, "Bound"))
				show_string(buf);
			//cut strings on \n
			ix = 0;
		}
		else{
			buf[ix++] = c;
		}
	}

}
