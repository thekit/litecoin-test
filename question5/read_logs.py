#!env python3

with(open("logs_from_ltc.log")) as flog:
    for line in flog:
        line = line.rstrip()
        if "Bound" in line:
            split = line.split()
            time = split[0].split("T")[1][:-1]
            bind = split[3]
            print(f"Bound {bind} at {time}")
